package com.karonori.filter;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaRecorder;

public class Player implements Runnable {
	// サンプリング周波数は自由には設定できないようだ
	// protected final int frequency = 8000;
	protected final int frequency = 44100;
	// protected final int frequency = 11025;
	// Changing the sample resolution changes sample type. byte vs. short.
	// ここを可変にするならテンプレートでも使ってshortとbyteを可変にしないとなー
	protected final int audio_encoding = AudioFormat.ENCODING_PCM_16BIT;

	protected final int channel_configuration = AudioFormat.CHANNEL_CONFIGURATION_MONO;

	protected static final int AUDIO_SOURCE = MediaRecorder.AudioSource.MIC;
	protected static final int AUDIO_STREAM_TYPE = AudioManager.STREAM_MUSIC;
	protected volatile boolean is_paused = false;
	protected volatile boolean is_playing;
	protected final Object mutex = new Object();

	protected AudioTrack at;
	protected AudioRecord ar;
	// run が実行されるたびに確保するのは無駄と思われたのでメンバにして初期化回数を減らす
	// パススルーを本分とするとこのクラスにこの属性はいらない．IIR専門のクラスを作るべきだ
	// となると入力と出力の最後の数点を次元数?分毎回渡す必要がある
	private short[] inputBuffer;
	protected Filterable filter;
	protected int buffer_size;

	public Player(Filterable f) {
		super();
		// We’re important…
		android.os.Process
				.setThreadPriority(android.os.Process.THREAD_PRIORITY_URGENT_AUDIO);
		this.filter = f;
		initializeDevice();
	}

	/**
	 * もしかしたらsleepからの復帰後に初期化しなくちゃいけないのかもしれないようものをまとめた コンストラクに直で書いてていいのかも
	 */
	public void initializeDevice() {
		int buffer_size = AudioRecord.getMinBufferSize(this.frequency,
				this.channel_configuration, this.audio_encoding);
		this.buffer_size = buffer_size;
		// 1秒間のサンプル数(1/sec) / 1度に処理できるサンプル数(1/op)
		// = 1秒間の処理数(op/sec)
		// example. 44100 / 4096 = 10.7666015625
		inputBuffer = new short[buffer_size];
		ar = new AudioRecord(AUDIO_SOURCE, this.frequency,
				this.channel_configuration, this.audio_encoding, buffer_size);
		at = new AudioTrack(AUDIO_STREAM_TYPE, this.frequency,
				this.channel_configuration, this.audio_encoding, buffer_size,
				AudioTrack.MODE_STATIC);

		at.setPlaybackPositionUpdateListener(new AudioTrack.OnPlaybackPositionUpdateListener() {
			public void onPeriodicNotification(AudioTrack track) {
			}

			// 再生完了時のコールバック
			public void onMarkerReached(AudioTrack track) {
				if (track.getPlayState() == AudioTrack.PLAYSTATE_PLAYING) {
					track.stop();// 再生完了したので発声停止
				}
			}
		});
	}

	/*
	 * public void finalize() { at.release(); ar.release(); at = null; ar =
	 * null; inputBuffer = null; }
	 */

	public void run() {
		// Wait until we’re recording…
		synchronized (mutex) {
			while (!this.is_playing) {
				try {
					mutex.wait();
				} catch (InterruptedException e) {
					throw new IllegalStateException("Wait() interrupted!", e);
				}
			}
		}

		// Allocate Recorder and Start Recording…
		int bufferRead = 0;
		ar.startRecording();
		while (this.is_playing) {
			synchronized (mutex) {
				if (this.is_paused) {
					try {
						mutex.wait(250);
					} catch (InterruptedException e) {
						throw new IllegalStateException("Wait() interrupted!",
								e);
					}
					continue;
				}
			}
			// 遅延を測るならreadの前後とフィルタ処理の前後の時間を測る
			bufferRead = ar.read(inputBuffer, 0, inputBuffer.length);

			if (bufferRead == AudioRecord.ERROR_INVALID_OPERATION) {
				throw new IllegalStateException(
						"read() returned AudioRecord.ERROR_INVALID_OPERATION");
			} else if (bufferRead == AudioRecord.ERROR_BAD_VALUE) {
				throw new IllegalStateException(
						"read() returned AudioRecord.ERROR_BAD_VALUE");
			} else if (bufferRead == AudioRecord.ERROR_INVALID_OPERATION) {
				throw new IllegalStateException(
						"read() returned AudioRecord.ERROR_INVALID_OPERATION");
			}
			at.write(filter.filter(inputBuffer), 0, inputBuffer.length);
			at.play();
			// stop と reloadStaticData はいらない気がするが，呼ばないと音が出力されなかった
			at.stop();
			at.reloadStaticData();
		}
		at.release();
		ar.release();

	}

	/**
	 * @param isPlaying
	 *            the isRecording to set
	 */
	public void setPlaying(boolean isPlaying) {
		synchronized (mutex) {
			this.is_playing = isPlaying;
			if (this.is_playing) {
				mutex.notify();
			}
		}
	}

	/**
	 * @return the isRecording
	 */
	public boolean isPlaying() {
		synchronized (mutex) {
			return is_playing;
		}
	}

	/**
	 * @param isPaused
	 *            the isPaused to set
	 */
	public void setPaused(boolean isPaused) {
		synchronized (mutex) {
			this.is_paused = isPaused;
		}
	}

	/**
	 * @return the isPaused
	 */
	public boolean isPaused() {
		synchronized (mutex) {
			return is_paused;
		}
	}

}
