/**
 *
 */
package com.karonori.filter.Short;

import java.util.Arrays;

import org.junit.Test;

/**
 * @author karo
 *
 */
public class IIRTest {
	@Test
	public void testFilter() throws Exception {
		float[] ic = { 4.0f };
		float[] oc = { 2.0f, 3.0f };
		IIR f = new IIR(1, 1.0f, ic, oc);
		short[] in = { 1, 2, 1, 0, 1 };
		short[] out = f.filter(in);
		short[] out_expected = { 2, -1, 12, -45, 182 };
		System.out.println(Arrays.toString(out));
		System.out.println(Arrays.toString(out_expected));
		org.junit.Assert.assertArrayEquals(out_expected, out);
	}

	@Test
	public void testFilter1() throws Exception {
		float[] ic = { 5.0f };
		float[] oc = { 4.0f, 6.0f };
		IIR f = new IIR(1, 0.5f, ic, oc);
		short[] in = { 8, 6, 10, -4};
		short[] out = f.filter(in);
		short[] out_expected = { 16, -44, 258, -1268 };
		System.out.println(Arrays.toString(out));
		System.out.println(Arrays.toString(out_expected));
		org.junit.Assert.assertArrayEquals(out_expected, out);
	}
}
