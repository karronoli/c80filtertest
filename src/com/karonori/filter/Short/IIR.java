package com.karonori.filter.Short;
import com.karonori.filter.Filterable;

/**
 * @author karo
 *
 */
public class IIR implements Filterable {
	protected int order;
	protected short[] in_coeff;
	protected short[] out_coeff;

	protected short[] buffer;
	private float scaling;
	public IIR (int order, float scaling, float[] ic, float[] oc) {
		this.order = order;
		this.scaling = scaling;
		this.buffer = new short[order + 1];
		short[] _ic = new short[order];
		for (int i = 0; i < ic.length; ++i) {
			_ic[i] = (short) (ic[i]);
		}
		this.in_coeff = _ic;

		short[] _oc = new short[order + 1];
		for (int i = 0; i < oc.length; ++i) {
			//あのサービスで出てくる係数はスケーリング済って，そうじゃなきゃそのまま使えないわな
			//_oc[i] = (short) (oc[i] / scaling);
			_oc[i] = (short) (oc[i]);
		}
		this.out_coeff = _oc;
	}


	public short[] filter (short[] in) {
		short[] out = new short[in.length];

		for (int i = 0, l = in.length; i < l; ++i) {
		    int tmp = (short)(in[i] * scaling);
		    for (int m = 0; m < order; ++m) {
				tmp = tmp - in_coeff[m] * buffer[m + 1];
			}
		    buffer[0] = (short)tmp;
		    tmp = 0;
		    for (int m = 0; m <= order; ++m) {
		    	tmp = tmp + out_coeff[m] * buffer[m];
			}
		    out[i] = (short)(tmp);
			for (int m = order; m > 0; --m) {
				buffer[m] = buffer[m - 1];
			}
		}

		return out;
	}
}
