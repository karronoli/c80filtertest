/**
 *
 */
package com.karonori.filter.Float;

import com.karonori.filter.Filterable;

/**
 * @author karo
 *
 */
public class IIR2 implements Filterable{
	protected IIR iir1;
	protected IIR iir2;
	protected IIR iir3;
	protected IIR iir4;
	protected IIR iir5;
	protected IIR iir6;

	public IIR2 () {
		float[] iir1_ic = {-6.69361321756255068e-01f};
		float[] iir1_oc = {3.96125960191644699e-01f, 3.96125960191644699e-01f};
		iir1 = new IIR(1, 4.17340330439063767e-01f, iir1_ic, iir1_oc);
		float[] iir2_ic = {-1.35385746539827401e+00f, 4.64415331356874905e-01f};
		float[] iir2_oc = {1.56915776337752472e-01f, 3.13831552675504943e-01f, 1.56915776337752472e-01f};
		iir2 = new IIR(2, 1.76142049797196804e-01f, iir2_ic, iir2_oc);

		float[] iir3_ic = {-1.40005995117868576e+00f, 5.14390775783536758e-01f};
		float[] iir3_oc = {1.56915776337752472e-01f, 3.13831552675504943e-01f, 1.56915776337752472e-01f};
		iir3 = new IIR(2, 1.82153170435138900e-01f, iir3_ic, iir3_oc);

		float[] iir4_ic = {-1.47966150321029599e+00f, 6.00492700228441367e-01f};
		float[] iir4_oc = {1.56915776337752472e-01f, 3.13831552675504943e-01f, 1.56915776337752472e-01f};
		iir4 = new IIR(2, 1.92509637714921078e-01f, iir4_ic, iir4_oc);

		float[] iir5_ic = {-1.59624943836078503e+00f, 7.26601366797260173e-01f};
		float[] iir5_oc = {1.56915776337752472e-01f, 3.13831552675504943e-01f, 1.56915776337752472e-01f};
		iir5 = new IIR(2, 2.07678175322377812e-01f, iir5_ic, iir5_oc);

		float[] iir6_ic = {-1.75386627650826021e+00f, 8.97089413111099465e-01f};
		float[] iir6_oc = {1.56915776337752472e-01f, 3.13831552675504943e-01f, 1.56915776337752472e-01f};
		iir6 = new IIR(2, 2.28184730601209973e-01f, iir6_ic, iir6_oc);

	}

	public short[] filter (short[] in) {
		short [] out = new short[in.length];
		out = iir6.filter(iir5.filter(iir4.filter(iir3.filter(iir2.filter(iir1.filter(in))))));
		return out;

	}

}
