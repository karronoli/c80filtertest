/**
 *
 */
package com.karonori.filter.Float;
import com.karonori.filter.Filterable;

/**
 * @author karo
 *
 */
public class IIR implements Filterable {
	protected int order;
	protected float[] in_coeff;
	protected float[] out_coeff;

	protected float[] buffer;
	private float scaling;
	public IIR (int order, float scaling, float[] ic, float[] oc) {
		this.order = order;
		this.scaling = scaling;
		this.buffer = new float[order + 1];
		this.in_coeff = ic;
		this.out_coeff = oc;
	}
	public short[] filter (short[] in) {
		short[] out = new short[in.length];

		for (int i = 0, l = in.length; i < l; ++i) {
		    float tmp = in[i] * scaling;
		    for (int m = 0; m < order; ++m) {
				tmp = tmp - in_coeff[m] * buffer[m + 1];
			}
		    buffer[0] = tmp;
		    tmp = 0;
		    for (int m = 0; m <= order; ++m) {
		    	tmp = tmp + out_coeff[m] * buffer[m];
			}
		    out[i] = (short)(tmp);
			for (int m = order; m > 0; --m) {
				buffer[m] = buffer[m - 1];
			}
		}

		return out;
	}


}
