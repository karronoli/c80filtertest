package com.karonori.filter;

import java.lang.reflect.Method;
import java.util.Date;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class Filter extends Activity {
	protected Method setFullScreenMode;
	protected Player player;
	protected Thread player_thread;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		setFullScreen();
		setListener();
	}

	@Override
	public void onResume() {
		super.onResume();
		try {
			setFullScreenMode.invoke(null, true);
		} catch (Exception o) {
			Log.d("is01fullscreen", "failed");
		}
		// とりあえず開始前の時刻表示
		TextView tv1 = (TextView) findViewById(R.id.textView1);
		tv1.setText((new Date()).toString());
	}

	@Override
	public void onStop() {
		super.onStop();
		stopFilter();
	}

	protected int setListener() {
		final Button btn1 = (Button) findViewById(R.id.button1);
		btn1.setOnClickListener(new OnClickListener() {

			class ThruFilter implements Filterable {
				public short[] filter(short[] in) {
					return in;
				}
			}

			@Override
			public void onClick(View arg0) {
				startFilter(new ThruFilter());
			}
		});

		final Button btn2 = (Button) findViewById(R.id.button2);
		btn2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				int order = 2;
				float scaling = 4.80579843935809853e-02f;
				float[] in_coeff = { 4.36535264249343746e-01f,
						2.05463014546639428e-01f };
				float[] out_coeff = { 8.54175585761406708e+00f,
						1.70835117152281342e+01f, 8.54175585761406708e+00f };
				startFilter(new com.karonori.filter.Short.IIR(order, scaling,
						in_coeff, out_coeff));
			}
		});

		final Button btn4 = (Button) findViewById(R.id.button4);
		btn4.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				startFilter(new com.karonori.filter.Float.IIR2());
			}
		});

		Button btn3 = (Button) findViewById(R.id.button3);
		btn3.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				stopFilter();
			}
		});

		Button btn5 = (Button) findViewById(R.id.button5);
		btn5.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				finish();
			}
		});

		return 0;
	}

	protected void startFilter(Filterable f) {
		player = new Player(f);
		player_thread = new Thread(player);
		player_thread.start();
		player.setPlaying(true);
	}

	protected void stopFilter() {
		player.setPlaying(false);
		try {
			player_thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * IS01の右側を消す ステータスバー等はいじらない 参考 http://blog.kcrt.net/2010/08/17/014820
	 */
	protected void setFullScreen() {
		try {
			Class<?> sgManager = Class
					.forName("jp.co.sharp.android.softguide.SoftGuideManager");
			Class<?> paramstype[] = { boolean.class };
			setFullScreenMode = sgManager.getMethod("setFullScreenMode",
					paramstype);
		} catch (Exception o) {
			Log.d("is01fullscreen", "failed" + o.getMessage() + ":"
					+ o.getClass().toString());
		}
	}

}

