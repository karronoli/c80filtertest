/**
 *
 */
package com.karonori.filter;

/**
 * @author karo
 *
 */
public interface Filterable {
	// 実処理を直接execute内に書くでもいいけどprivate staticでinline的効果を狙うなら
	// executeからクラス独自の_executeを呼ぶというのもアリかも
	public short[] filter(short[] in);
}
